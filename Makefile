CFLAGS = `sdl2-config --cflags` -g -std=c++14 -I/usr/local/opt/zlib/include
CC = clang++

buildDir = `pwd`/build
sourceDir = `pwd`/src
dependencies = `sdl2-config --libs` -lSDL2_image -lSDL2_mixer -L/usr/local/opt/zlib/lib -lz

.DEFAULT_GOAL := game

game: build-directory
	$(CC) $(CFLAGS) \
	$(dependencies) \
	$(sourceDir)/animated_graphic.cpp \
	$(sourceDir)/base64.cpp \
	$(sourceDir)/enemy.cpp \
	$(sourceDir)/game.cpp \
	$(sourceDir)/game_object_factory.cpp \
	$(sourceDir)/game_over_state.cpp \
	$(sourceDir)/game_state.cpp \
	$(sourceDir)/game_state_machine.cpp \
	$(sourceDir)/input_handler.cpp \
	$(sourceDir)/level.cpp \
	$(sourceDir)/level_parser.cpp \
	$(sourceDir)/logger.cpp \
	$(sourceDir)/main.cpp \
	$(sourceDir)/main_menu_state.cpp \
	$(sourceDir)/menu_button.cpp \
	$(sourceDir)/menu_state.cpp \
	$(sourceDir)/pause_state.cpp \
	$(sourceDir)/play_state.cpp \
	$(sourceDir)/player.cpp \
	$(sourceDir)/sdl_game_object.cpp \
	$(sourceDir)/state_parser.cpp \
	$(sourceDir)/texture_manager.cpp \
	$(sourceDir)/sound_manager.cpp \
	$(sourceDir)/tile_layer.cpp \
	$(sourceDir)/object_layer.cpp \
	$(sourceDir)/tinyxml2.cpp \
	-o $(buildDir)/game

clean:
	rm -rf $(buildDir)/*

build-directory:
	@mkdir -p $(buildDir)

run: game
	$(buildDir)/game

debug: game
	lldb $(buildDir)/game

all: clean game

help:
	@echo "Available tasks (default is marked with *):"
	@echo "*all	equivalent to 'clean' then 'game'"
	@echo " clean	remove files and directories generated during compilation"
	@echo " game	compile the game"
	@echo " run	compile then run the game"
	@echo " debug	compile then launch a debugger (lldb)"
	@echo " help	show this help message"
	@echo ""
	@echo "Usage exemple:"
	@echo "To compile and run the game, be sure to be at the root of the project and execute the command \`make run\`."
