#ifndef __SDLGameObject__
#define __SDLGameObject__

#include <string>
#include "game_object.hpp"
#include "loader_params.hpp"
#include "vector_2d.hpp"

class SDLGameObject: public GameObject {
public:
  SDLGameObject();

  virtual void draw();
  virtual void update();
  virtual void clean()=0;
  virtual void load(const LoaderParams* pParams);

  Vector2D getPosition() {return position;}
  Vector2D getVelocity() {return velocity;}
  Vector2D getAcceleration() {return acceleration;}
  int getWidth() {return width;}
  int getHeight() {return height;}

protected:
  Vector2D position;
  Vector2D velocity;
  Vector2D acceleration;
  int width;
  int height;
  int currentRow;
  int currentFrame;
  int numFrames;
  std::string textureId;
};

#endif /* defined(__SDLGameObject__) */
