#include "level_parser.hpp"
#include "texture_manager.hpp"
#include "game.hpp"
#include "game_object_factory.hpp"
#include "tile_layer.hpp"
#include "object_layer.hpp"
#include "base64.h"
#include <zlib.h>
#include <iostream>
#include "string.hpp"

Level* LevelParser::parseLevel(const char *levelFile) {
  // Create a TinyXML document and load the map XML
  tinyxml2::XMLDocument levelDocument;
  levelDocument.LoadFile(levelFile);

  if (levelDocument.Error()) {
    std::cerr << "Error loading file: " << levelFile << std::endl;
    std::cerr << levelDocument.GetErrorStr1() << std::endl;
    std::cerr << levelDocument.GetErrorStr2() << std::endl;
  }

  // Create the level object
  Level* pLevel = new Level();

  // Get the root node
  tinyxml2::XMLElement* pRoot = levelDocument.RootElement();

  tileSize = pRoot->IntAttribute("tilewidth");
  width = pRoot->IntAttribute("width");
  height = pRoot->IntAttribute("height");

  tinyxml2::XMLElement* pProperties = pRoot->FirstChildElement();
  for (tinyxml2::XMLElement* e = pProperties->FirstChildElement();
       e != NULL;
       e = e->NextSiblingElement()) {
    if (e->Value() == std::string("property")) {
      parseTextures(e);
    }
  }

  // Parse the tilesets
  for (tinyxml2::XMLElement* e = pRoot->FirstChildElement();
       e != NULL;
       e = e->NextSiblingElement()) {
    if (e->Value() == std::string("tileset")) {
      parseTilesets(e, pLevel->getTilesets());
    }
  }

  // Parse any object layers
  for (tinyxml2::XMLElement* e = pRoot->FirstChildElement();
       e != NULL;
       e = e->NextSiblingElement()) {

    if (e->Value() == std::string("objectgroup") ||
        e->Value() == std::string("layer")) {
      if (e->BoolAttribute("visible", true)) {
        if (e->FirstChildElement()->Value() == std::string("object")) {
          parseObjectLayer(e, pLevel->getLayers());
        } else if (e->FirstChildElement()->Value() == std::string("data")) {
          parseTileLayer(e, pLevel->getLayers(), pLevel->getTilesets());
        }
      }
    }
  }

  return pLevel;
}

void LevelParser::parseTilesets(tinyxml2::XMLElement* pTilesetRoot,
                                std::vector<Tileset>* pTilesets) {
  // Add the tileset to the texture manager
  std::string sourceFile = pTilesetRoot->FirstChildElement()->Attribute("source");
  std::string name = pTilesetRoot->Attribute("name");

  TextureManager::instance()->load(sourceFile, name, Game::instance()->getRenderer());

  // Create a tileset object
  Tileset tileset;
  tileset.width = pTilesetRoot->FirstChildElement()->IntAttribute("width");
  tileset.height = pTilesetRoot->FirstChildElement()->IntAttribute("height");
  tileset.firstGridID = pTilesetRoot->IntAttribute("firstgid");
  tileset.name = name;
  tileset.source = sourceFile;
  tileset.tileWidth = pTilesetRoot->IntAttribute("tilewidth");
  tileset.tileHeight = pTilesetRoot->IntAttribute("tileheight");
  tileset.spacing = pTilesetRoot->IntAttribute("spacing");
  tileset.margin = pTilesetRoot->IntAttribute("margin");
  tileset.numColumns = pTilesetRoot->IntAttribute("columns");

  pTilesets->push_back(tileset);
}

void LevelParser::parseTileLayer(tinyxml2::XMLElement *pTileElement,
                                 std::vector<Layer *> *pLayers,
                                 const std::vector<Tileset> *pTilesets) {

  TileLayer* pTileLayer = new TileLayer(tileSize, *pTilesets);
  pTileLayer->setLayerId(pTileElement->Attribute("name"));
  pTileLayer->setWidth(pTileElement->IntAttribute("width"));
  pTileLayer->setHeight(pTileElement->IntAttribute("height"));

  // Tile data
  std::vector<std::vector<int> > data;

  // Base 64 decode
  std::string decodedIds;
  tinyxml2::XMLElement* pDataNode;

  for (tinyxml2::XMLElement* e = pTileElement->FirstChildElement();
       e != NULL;
       e = e->NextSiblingElement()) {
    if (e->Value() == std::string("data")) {
      pDataNode = e;
    }
  }

  for (tinyxml2::XMLNode* n = pDataNode->FirstChild();
       n != NULL;
       n = n->NextSibling()) {
    tinyxml2::XMLText* text = n->ToText();
    std::string t = text->Value();
    decodedIds = base64_decode(trim(t));
  }

  if (decodedIds == "") {
    std::cerr << "Error decoding base64 level, result is an empty string" << std::endl;
  }

  // Uncompress zlib compression
  uLongf numGids = width * height * sizeof(int);
  std::vector<unsigned> gids(numGids);

  uncompress((Bytef*) &gids[0],
             &numGids,
             (const Bytef*) decodedIds.c_str(),
             decodedIds.size());

  // Create rows
  std::vector<int> layerRow(width);
  for (int j = 0; j < height; j++) {
    data.push_back(layerRow);
  }

  // Fill our 2D data structure
  for (int rows = 0; rows < height; rows++) {
    for (int cols = 0; cols < width; cols++) {
      data[rows][cols] = gids[(rows * width) + cols];
    }
  }

  pTileLayer->setTileIds(data);
  pLayers->push_back(pTileLayer);
}

void LevelParser::parseTextures(tinyxml2::XMLElement *pTextureRoot) {
  TextureManager::instance()->load(pTextureRoot->Attribute("value"),
                                   pTextureRoot->Attribute("name"),
                                   Game::instance()->getRenderer());
}

void LevelParser::parseObjectLayer(tinyxml2::XMLElement *pObjectElement,
                                   std::vector<Layer *> *pLayers) {
  ObjectLayer* pObjectLayer = new ObjectLayer();
  pObjectLayer->setLayerId(pObjectElement->Attribute("name"));
  pObjectLayer->setWidth(pObjectElement->IntAttribute("width"));
  pObjectLayer->setHeight(pObjectElement->IntAttribute("height"));

  for (tinyxml2::XMLElement* e = pObjectElement->FirstChildElement();
       e != NULL;
       e = e->NextSiblingElement()) {

    if (e->Value() == std::string("object")) {

      // Get the initial node values type, x and y
      int x = e->IntAttribute("x");
      int y = e->IntAttribute("y");
      std::string type = e->Attribute("type");

      GameObject* pGameObject = GameObjectFactory::instance()->create(type);

      // Get property values
      int numFrames, callbackId, animSpeed, textureWidth, textureHeight;
      numFrames = callbackId = animSpeed = textureWidth = textureHeight = 0;
      std::string textureId;

      for (tinyxml2::XMLElement* properties = e->FirstChildElement();
           properties != NULL;
           properties = properties->NextSiblingElement()) {

        if (properties->Value() == std::string("properties")) {
          for (tinyxml2::XMLElement* property = properties->FirstChildElement();
               property != NULL;
               property = property->NextSiblingElement()) {

            if (property->Value() == std::string("property")) {
              std::string propName = property->Attribute("name");

              if (propName == "numFrames") {
                numFrames = property->IntAttribute("value");
              } else if (propName == "textureWidth") {
                textureWidth = property->IntAttribute("value");
              } else if (propName == "textureHeight") {
                textureHeight = property->IntAttribute("value");
              } else if (propName == "textureId") {
                textureId = property->Attribute("value");
              } else if (propName == "callbackId") {
                callbackId = property->IntAttribute("value");
              } else if (propName == "animSpeed") {
                animSpeed = property->IntAttribute("value");
              }
            }
          }
        }
      }

      std::cout << "LevelParser::parseObjectLayer():"
                << " type=" << type
                << " x=" << x
                << " y=" << y
                << " tWidth=" << textureWidth
                << " tHeight=" << textureHeight
                << " tId=" << textureId
                << " numFrames=" << numFrames
                << " clbckId=" << callbackId
                << " animSpeed=" << animSpeed
                << std::endl;

      pGameObject->load(new LoaderParams(x, y, textureWidth, textureHeight, textureId,
                                         numFrames, callbackId, animSpeed));
      pObjectLayer->getGameObjects()->push_back(pGameObject);
    }
  }

  pLayers->push_back(pObjectLayer);
}
