#ifndef __InputHandler__
#define __InputHandler__

#include <vector>
#include <utility>
#include <SDL.h>
#include "vector_2D.hpp"

enum MouseButtons {
  MOUSE_LEFT = 0,
  MOUSE_MIDDLE = 1,
  MOUSE_RIGHT = 2
};

class InputHandler {
public:
  static InputHandler* instance();
  void update();
  void clean();
  void initializeJoysticks();
  bool joysticksInitialized() {return areJoysticksInitialized;}
  int xvalue(int joy, int stick);
  int yvalue(int joy, int stick);
  bool getButtonState(int joy, int buttonNumber);
  bool getMouseButtonState(int buttonNumber);
  Vector2D* getMousePosition();
  bool isKeyDown(SDL_Scancode key);
  void reset();

private:
  InputHandler();
  ~InputHandler();

  static InputHandler* pInstance;
  std::vector<SDL_Joystick*> joysticks;
  std::vector<std::pair<Vector2D*, Vector2D*> > joystickValues;
  std::vector<std::vector<bool> > buttonStates;
  std::vector<bool> mouseButtonStates;
  bool areJoysticksInitialized;
  const int joystickDeadZone = 10000;
  Vector2D* mousePosition;
  Uint8* keystates;

  void onKeyDown();
  void onKeyUp();

  void onMouseMove(SDL_Event& event);
  void onMouseButtonDown(SDL_Event& event);
  void onMouseButtonUp(SDL_Event& event);

  void onJoystickAxisMove(SDL_Event& event);
  void onJoystickButtonDown(SDL_Event& event);
  void onJoystickButtonUp(SDL_Event& event);
};

#endif /* defined(__InputHandler__) */
