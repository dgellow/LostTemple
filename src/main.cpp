#include "game.hpp"
#include <iostream>
#include "const.hpp"
#include "tinyxml2.hpp"

int main (int argc, char* argv[]) {
  Uint32 frameStart = 0;
  Uint32 frameTime = 0;

  Game::instance()->init("Lost Temple", 100, 100, 640, 480, false);

  while (Game::instance()->running()) {
    Game::instance()->handleEvents();
    Game::instance()->update();
    Game::instance()->render();

    frameTime = SDL_GetTicks() - frameStart;

    if (frameTime < DELAY_TIME)  {
      SDL_Delay((int) (DELAY_TIME - frameTime));
    }

    SDL_Delay(10);
  }

  Game::instance()->clean();

  return 0;
}
