#ifndef __MenuButton__
#define __MenuButton__

#include "sdl_game_object.hpp"
#include "loader_params.hpp"
#include "base_creator.hpp"

class MenuButton : public SDLGameObject {
public:
  MenuButton();

  virtual void draw();
  virtual void update();
  virtual void clean();
  virtual void load(const LoaderParams* pParams);
  void setCallback(void(*callback) ()) {this->callback = callback;}
  int getCallbackId() {return callbackId;}

private:
  enum button_state {
    MOUSE_OUT = 0,
    MOUSE_OVER = 1,
    CLICKED = 2
  };

  void (*callback) ();
  int callbackId;
  bool isReleased;
};

class MenuButtonCreator : public BaseCreator {
private:
  GameObject* createGameObject() const {
    return new MenuButton();
  }
};

#endif /* defined(__MenuButton__) */
