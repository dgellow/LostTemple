#include "game.hpp"
#include "const.hpp"
#include "texture_manager.hpp"
#include "input_handler.hpp"
#include "main_menu_state.hpp"
#include "play_state.hpp"
#include "menu_button.hpp"
#include "animated_graphic.hpp"
#include "player.hpp"
#include "enemy.hpp"
#include "game_object_factory.hpp"

Game* Game::pInstance = 0;

Game* Game::instance() {
  if (pInstance == 0) {
    pInstance = new Game();
  }
  return pInstance;
}

Game::Game() {
  isRunning = false;
}

bool Game::init(const char* title, int xpos, int ypos,
                 int width, int height, bool fullscreen) {

  gameWidth = width;
  gameHeight = height;

  if (!initSDL(title, xpos, ypos, fullscreen)) {
    return false;
  }

  // Initialize inputs
  InputHandler::instance()->initializeJoysticks();

  // Init game object factory
  GameObjectFactory::instance()->registerType("MenuButton", new MenuButtonCreator());
  GameObjectFactory::instance()->registerType("AnimatedGraphic", new AnimatedGraphicCreator());
  GameObjectFactory::instance()->registerType("Player", new PlayerCreator());
  GameObjectFactory::instance()->registerType("Enemy", new EnemyCreator());

  // Init state machine
  pGameStateMachine = new GameStateMachine();
  pGameStateMachine->pushState(new MainMenuState());

  // Done
  std::cout << "Game init success" << std::endl;
  isRunning = true;

  return true;
}

bool Game::initSDL(const char* title, int xpos, int ypos, bool fullscreen) {
  // Init SDL
  int flags = 0;

  if (fullscreen) {
    flags = SDL_WINDOW_FULLSCREEN;
  }

  if (SDL_Init(SDL_INIT_EVERYTHING) == 0)  {
    std::cout << "SDL init success" << std::endl;
    pWindow = SDL_CreateWindow(title, xpos, ypos, gameWidth, gameHeight, flags);

    if (pWindow != 0) {
      std::cout << "Window creation success" << std::endl;
      pRenderer = SDL_CreateRenderer(pWindow, -1, 0);

      if (pRenderer != 0) {
        std::cout << "Renderer creation success" << std::endl;
        SDL_SetRenderDrawColor(pRenderer,
                               COLOR_BG_R, COLOR_BG_G, COLOR_BG_R, COLOR_BG_A);
      } else {
        std::cerr << "Renderer init failed" << std::endl;
        return false;
      }
    } else {
      std::cerr << "Window init failed" << std::endl;
      return false;
    }
  } else {
    std::cerr << "SDL init failed" << std::endl;
    return false;
  }

  return true;
}

void Game::update() {
  pGameStateMachine->update();
}

void Game::render() {
  SDL_RenderClear(pRenderer);

  pGameStateMachine->render();

  SDL_RenderPresent(pRenderer);
}

void Game::handleEvents() {
  InputHandler::instance()->update();
  if (InputHandler::instance()->isKeyDown(SDL_SCANCODE_RETURN)) {
    pGameStateMachine->changeState(new PlayState());
  }
}

void Game::clean() {
  std::cout << "cleaning game" << std::endl;
  InputHandler::instance()->clean();
  SDL_DestroyWindow(pWindow);
  SDL_DestroyRenderer(pRenderer);
  SDL_Quit();
}

void Game::quit() {
  isRunning = false;
}
