#ifndef __GameStateMachine__
#define __GameStateMachine__

#include <vector>
#include "game_state.hpp"

class GameStateMachine {
public:
  void pushState(GameState* pState);
  void popState();
  void changeState(GameState* pState);
  void update();
  void render();

private:
  std::vector<GameState*> gameStates;
};

#endif /* defined(__GameStateMachine__) */
