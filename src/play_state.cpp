#include "play_state.hpp"
#include <iostream>
#include "texture_manager.hpp"
#include "state_parser.hpp"
#include "level_parser.hpp"
#include "game.hpp"
#include "player.hpp"
#include "enemy.hpp"
#include "input_handler.hpp"
#include "pause_state.hpp"
#include "game_over_state.hpp"

const std::string PlayState::stateId = "Play";

void PlayState::update() {
  if (InputHandler::instance()->isKeyDown(SDL_SCANCODE_ESCAPE)) {
    Game::instance()->getStateMachine()->pushState(new PauseState());
  }

  pLevel->update();

  // if (checkCollision(dynamic_cast<SDLGameObject*> (gameObjects[0]),
  //                    dynamic_cast<SDLGameObject*> (gameObjects[1]))) {
  //   Game::instance()->getStateMachine()->pushState(new GameOverState());
  // }
}

void PlayState::render() {
  pLevel->render();
  // for (size_t i = 0; i < gameObjects.size(); i++) {
  //   gameObjects[i]->draw();
  // }
}

bool PlayState::onEnter() {
  LevelParser levelParser;
  pLevel = levelParser.parseLevel("data/level/test.tmx");

  // StateParser stateParser;
  // stateParser.parseState("data/test.xml", getStateId(), &gameObjects, &textureIdList);

  std::cout << "entering PlayState\n";
  return true;
}

bool PlayState::onExit() {
  // Clear the texture manager
  for (size_t i = 0; i < textureIdList.size(); i++) {
    TextureManager::instance()->clearFromTextureMap(textureIdList[i]);
  }

  std::cout << "exiting PlayState\n";
  return true;
}

bool PlayState::checkCollision(SDLGameObject* p1, SDLGameObject* p2) {
  int leftA, leftB;
  int rightA, rightB;
  int topA, topB;
  int bottomA, bottomB;

  // Calculate the sides of rect A
  leftA = p1->getPosition().getX();
  rightA = p1->getPosition().getX() + p1->getWidth();
  topA = p1->getPosition().getY();
  bottomA = p1->getPosition().getY() + p1->getHeight();

  // Calculate the sides of rect B
  leftB = p2->getPosition().getX();
  rightB = p2->getPosition().getX() + p2->getWidth();
  topB = p2->getPosition().getY();
  bottomB = p2->getPosition().getY() + p2->getHeight();

  // If any of the sides from A are outside of B
  if (bottomA <= topB ||
      topA >= bottomB ||
      rightA <= leftB ||
      leftA >= rightB) {
    return false;
  }

  return true;
}
