#ifndef __Enemy__
#define __Enemy__

#include "sdl_game_object.hpp"
#include "loader_params.hpp"
#include "base_creator.hpp"

class Enemy : public SDLGameObject {
public:
  Enemy();

  virtual void draw();
  virtual void update();
  virtual void clean();
  virtual void load(const LoaderParams* pParams);
};

class EnemyCreator : public BaseCreator {
private:
  GameObject* createGameObject() const {
    return new Enemy();
  }
};

#endif /* defined(__Enemy__) */
