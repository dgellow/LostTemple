#ifndef __PlayState__
#define __PlayState__

#include <vector>
#include "game_state.hpp"
#include "game_object.hpp"
#include "sdl_game_object.hpp"
#include "level.hpp"

class PlayState : public GameState {
public:
  virtual void update();
  virtual void render();
  virtual bool onEnter();
  virtual bool onExit();
  virtual std::string getStateId() const {return stateId;}

private:
  static const std::string stateId;
  bool checkCollision(SDLGameObject* p1, SDLGameObject* p2);
  Level* pLevel;
};

#endif /* defined(__PlayState__) */
