#ifndef __Logger__
#define __Logger__

#include <string>
#include "SDL.h"

class Logger {
public:
  static void logMouseButtonEvent(SDL_Event event);
  static void logKeyboardEvent(SDL_Event event);

private:
  static void logAudioDeviceEvent(SDL_Event event);
  static void logControllerAxisEvent(SDL_Event event);
  static void logControllerButtonEvent(SDL_Event event);
  static void logControllerDeviceEvent(SDL_Event event);
  static void logDollarGestureEvent(SDL_Event event);
  static void logDropEvent(SDL_Event event);
  static void logTouchFingerEvent(SDL_Event event);
  static void logJoyAxisEvent(SDL_Event event);
  static void logJoyBallEvent(SDL_Event event);
  static void logJoyButtonEvent(SDL_Event event);
  static void logJoyDeviceEvent(SDL_Event event);
  static void logJoyHatEvent(SDL_Event event);
  static void logMouseMotionEvent(SDL_Event event);
  static void logMouseWheelEvent(SDL_Event event);
  static void logMultiGestureEvent(SDL_Event event);
  static void logQuitEvent(SDL_Event event);
  static void logSysWMEvent(SDL_Event event);
  static void logTextEditingEvent(SDL_Event event);
  static void logTextInputEvent(SDL_Event event);
  static void logUserEvent(SDL_Event event);
  static void logWindowEvent(SDL_Event event);
  static void logCommonEvent(SDL_Event event);

  static void logEventInternal(Uint32 timestamp, std::string type, std::string str);
};

#endif /* defined(__Logger__) */
