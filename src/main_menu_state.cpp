#include "main_menu_state.hpp"
#include <iostream>
#include "texture_manager.hpp"
#include "game.hpp"
#include "state_parser.hpp"
#include "menu_button.hpp"
#include "play_state.hpp"

const std::string MainMenuState::stateId = "MainMenu";

bool MainMenuState::onEnter() {
  // Parse the state
  StateParser stateParser;
  stateParser.parseState("data/test.xml", getStateId(), &gameObjects, &textureIdList);

  callbacks.push_back(0);
  callbacks.push_back(menuToPlay);
  callbacks.push_back(exitFromMenu);

  // Set the callbacks from menu items
  setCallbacks(callbacks);

  std::cout << "entering MainMenuState\n";
  return true;
}

bool MainMenuState::onExit() {
  // Clear the texture manager
  for (size_t i = 0; i < textureIdList.size(); i++) {
    TextureManager::instance()->clearFromTextureMap(textureIdList[i]);
  }

  return true;
}

void MainMenuState::menuToPlay() {
  Game::instance()->getStateMachine()->changeState(new PlayState());
}

void MainMenuState::exitFromMenu() {
  Game::instance()->quit();
}

void MainMenuState::setCallbacks(const std::vector<Callback> &callbacks) {
  // Go through gameObjects
  for (size_t i = 0; i < gameObjects.size(); i++) {
    // If they are of type MenuButton then assign a callback based on
    // the id passed from the file
    if (dynamic_cast<MenuButton*>(gameObjects[i])) {
      MenuButton* pButton = dynamic_cast<MenuButton*>(gameObjects[i]);
      pButton->setCallback(callbacks[pButton->getCallbackId()]);
    }
  }
}
