#ifndef __Layer__
#define __Layer__

#include <vector>
#include <string>

class Layer {
public:
  virtual void render()=0;
  virtual void update()=0;
  virtual std::string getLayerId() const =0;
  virtual void setLayerId(std::string layerId) =0;
  virtual int getWidth() const =0;
  virtual void setWidth(int width) =0;
  virtual int getHeight() const =0;
  virtual void setHeight(int height) =0;

protected:
  virtual ~Layer() {}
};

#endif /* defined(__Layer__) */
