#include "game_over_state.hpp"
#include <iostream>
#include "game.hpp"
#include "texture_manager.hpp"
#include "state_parser.hpp"
#include "main_menu_state.hpp"
#include "play_state.hpp"
#include "animated_graphic.hpp"
#include "menu_button.hpp"

const std::string GameOverState::stateId = "GameOver";

void GameOverState::update() {
  for (size_t i = 0; i < gameObjects.size(); i++) {
    gameObjects[i]->update();
  }
}

void GameOverState::render() {
  for (size_t i = 0; i < gameObjects.size(); i++) {
    gameObjects[i]->draw();
  }
}

bool GameOverState::onEnter() {
  StateParser stateParser;
  stateParser.parseState("data/test.xml", getStateId(), &gameObjects, &textureIdList);

  callbacks.push_back(0);
  callbacks.push_back(gameOverToMain);
  callbacks.push_back(restartPlay);

  setCallbacks(callbacks);

  std::cout << "entering GameOverState\n";

  return true;
}

bool GameOverState::onExit() {
  // Clear the texture manager
  for (size_t i = 0; i < textureIdList.size(); i++) {
    TextureManager::instance()->clearFromTextureMap(textureIdList[i]);
  }

  return true;
}

void GameOverState::gameOverToMain() {
  Game::instance()->getStateMachine()->changeState(new MainMenuState());
}

void GameOverState::restartPlay() {
  Game::instance()->getStateMachine()->changeState(new PlayState());
}

void GameOverState::setCallbacks(const std::vector<Callback> &callbacks) {
    for (size_t i = 0; i < gameObjects.size(); i++) {
    if (dynamic_cast<MenuButton*>(gameObjects[i])) {
      MenuButton* pButton = dynamic_cast<MenuButton*>(gameObjects[i]);
      pButton->setCallback(callbacks[pButton->getCallbackId()]);
    }
  }
}
