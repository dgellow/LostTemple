#ifndef __BaseCreator__
#define __BaseCreator__

#include "game_object.hpp"

class BaseCreator {
public:
  virtual GameObject* createGameObject() const =0;
  virtual ~BaseCreator() {}
};

#endif /* defined(__BaseCreator__) */
