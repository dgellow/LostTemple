#include "player.hpp"
#include <SDL.h>
#include "input_handler.hpp"

Player::Player() : SDLGameObject() {

}

void Player::load(const LoaderParams* pParams) {
  SDLGameObject::load(pParams);
}

void Player::draw() {
  SDLGameObject::draw();
}

void Player::update() {
  velocity.setX(0);
  velocity.setY(0);

  Vector2D* mousePosition = InputHandler::instance()->getMousePosition();
  velocity = (*mousePosition - position) / 100;

  handleInput();
  currentFrame = int((SDL_GetTicks() / 100) % (numFrames < 1 ? 1: numFrames));

  SDLGameObject::update();
}

void Player::clean() {
  SDLGameObject::clean();
}

void Player::handleInput() {
  Vector2D* target = InputHandler::instance()->getMousePosition();
  velocity = *target - position;
  velocity /= 50;
}
