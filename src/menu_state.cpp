#include "menu_state.hpp"
#include <iostream>
#include "texture_manager.hpp"
#include "game.hpp"
#include "menu_button.hpp"
#include "play_state.hpp"

void MenuState::update() {
  // std::cout << ".——————————————————" << std::endl;
  // std::cout << "|" << std::endl;
  // std::cout << "tick MenuState::update()" << std::endl;
  // std::cout << "gameObjects.size(): " << gameObjects.size() << std::endl;

  for (size_t i = 0; i < gameObjects.size(); i++) {
    gameObjects[i]->update();
  }

  // std::cout << "|" << std::endl;
  // std::cout << "^——————————————————" << std::endl;
}

void MenuState::render() {
  for (size_t i = 0; i < gameObjects.size(); i++) {
    gameObjects[i]->draw();
  }
}

bool MenuState::onEnter() {
  std::cout << "entering MenuState\n";
  return true;
}

bool MenuState::onExit() {
  std::cout << "exiting MenuState\n";
  return true;
}
