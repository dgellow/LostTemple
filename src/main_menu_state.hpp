#ifndef __MainMenuState__
#define __MainMenuState__

#include <vector>
#include "menu_state.hpp"
#include "game_object.hpp"

class MainMenuState : public MenuState {
public:
  virtual ~MainMenuState() {}

  virtual bool onEnter();
  virtual bool onExit();
  virtual std::string getStateId() const {return stateId;}

private:
  virtual void setCallbacks(const std::vector<Callback> &callbacks);

  // callbacks for menu items
  static void menuToPlay();
  static void exitFromMenu();

  static const std::string stateId;
};

#endif /* defined(__MainMenuState__) */
