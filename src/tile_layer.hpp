#ifndef __TileLayer__
#define __TileLayer__

#include <vector>
#include <string>
#include "level.hpp"
#include "layer.hpp"
#include "vector_2d.hpp"

class TileLayer : public Layer {
public:
  TileLayer(int tileSize, const std::vector<Tileset> &tilesets);

  virtual void update();
  virtual void render();

  std::string getLayerId() const {
    return layerId;
  }

  void setLayerId(std::string layerId) {
    this->layerId = layerId;
  }

  int getWidth() const {
    return width;
  }

  void setWidth(int width) {
    this->width = width;
  }

  int getHeight() const {
    return height;
  }

  void setHeight(int height) {
    this->height = height;
  }

  void setTileIds(const std::vector<std::vector<int> > &data) {
    tileIds = data;
  }

  void setTileSize(int tileSize) {
    this->tileSize = tileSize;
  }

  Tileset getTilesetById(int tileId);

private:
  int numColumns;
  int numRows;
  int tileSize;
  int height;
  int width;
  std::string layerId;

  Vector2D position;
  Vector2D velocity;

  const std::vector<Tileset> &tilesets;

  std::vector<std::vector<int> > tileIds;
};

#endif /* defined(__TileLayer__) */
