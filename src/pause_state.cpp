#include "pause_state.hpp"
#include <iostream>
#include "texture_manager.hpp"
#include "game.hpp"
#include "state_parser.hpp"
#include "main_menu_state.hpp"
#include "input_handler.hpp"
#include "menu_button.hpp"

const std::string PauseState::stateId = "Pause";

void PauseState::update() {
  for (size_t i = 0; i < gameObjects.size(); i++) {
    gameObjects[i]->update();
  }
}

void PauseState::render() {
  for (size_t i = 0; i < gameObjects.size(); i++) {
    gameObjects[i]->draw();
  }
}

bool PauseState::onEnter() {
  StateParser stateParser;
  stateParser.parseState("data/test.xml", getStateId(), &gameObjects, &textureIdList);

  callbacks.push_back(0);
  callbacks.push_back(resumePlay);
  callbacks.push_back(pauseToMain);

  setCallbacks(callbacks);

  std::cout << "entering PauseState\n";
  return true;
}

bool PauseState::onExit() {
  for (size_t i = 0; i < textureIdList.size(); i++) {
    TextureManager::instance()->clearFromTextureMap(textureIdList[i]);
  }

  InputHandler::instance()->reset();

  std::cout << "exiting PauseState\n";
  return true;
}

void PauseState::pauseToMain() {
  Game::instance()->getStateMachine()->changeState(new MainMenuState());
}

void PauseState::resumePlay() {
  Game::instance()->getStateMachine()->popState();
}

void PauseState::setCallbacks(const std::vector<Callback> &callbacks) {
    for (size_t i = 0; i < gameObjects.size(); i++) {
    if (dynamic_cast<MenuButton*>(gameObjects[i])) {
      MenuButton* pButton = dynamic_cast<MenuButton*>(gameObjects[i]);
      pButton->setCallback(callbacks[pButton->getCallbackId()]);
    }
  }
}
