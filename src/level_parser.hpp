#ifndef __LevelParser__
#define __LevelParser__

#include "tinyxml2.hpp"
#include "level.hpp"

class LevelParser {
public:
  Level* parseLevel(const char* levelFile);

private:
  void parseTilesets(tinyxml2::XMLElement* pTilesetRoot,
                     std::vector<Tileset>* pTilesets);

  void parseTileLayer(tinyxml2::XMLElement* pTileElement,
                      std::vector<Layer*> *pLayers,
                      const std::vector<Tileset>* pTilesets);

  void parseTextures(tinyxml2::XMLElement* pTextureRoot);

  void parseObjectLayer(tinyxml2::XMLElement* pObjectElement,
                        std::vector<Layer*>* pLayers);

  int tileSize;
  int width;
  int height;
};

#endif /* defined(__LevelParser__) */
