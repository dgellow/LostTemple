#include "game_object_factory.hpp"
#include <iostream>

GameObjectFactory* GameObjectFactory::pInstance = 0;

GameObjectFactory* GameObjectFactory::instance() {
  if (pInstance == 0) {
    pInstance = new GameObjectFactory();
  }
  return pInstance;
}

bool GameObjectFactory::registerType(std::string typeId, BaseCreator *pCreator) {
  std::cout << "typeId: " << typeId << std::endl;

  std::map<std::string, BaseCreator*>::iterator it = creators.find(typeId);

  // if the type is already registered, do nothing
  if (it != creators.end()) {
    delete pCreator;
    return false;
  }

  creators[typeId] = pCreator;

  std::cout << "creators.size(): " << creators.size() << std::endl;

  return true;
}

GameObject* GameObjectFactory::create(std::string typeId) {
  std::map<std::string, BaseCreator*>::iterator it = creators.find(typeId);

  if (it == creators.end()) {
    std::cout << "could not find type: " << typeId << "\n";
    return NULL;
  }

  std::cout << "Create object from factory, type: " << typeId << std::endl;

  BaseCreator* pCreator = (*it).second;
  return pCreator->createGameObject();
}
