#include "logger.hpp"
#include <iostream>

// void Logger::logEvent(SDL_Event event) {
//   std::string eventType = "";
//   void (*logFn) (std::string, SDL_Event);

//   switch (event.type) {

//   case SDL_AUDIODEVICEADDED:
//     eventType = "AudioDeviceAdded";
//     logFn = logAudioDeviceEvent;
//     break;

//   case SDL_AUDIODEVICEREMOVED:
//     eventType = "AudioDeviceRemoved";
//     logFn = logAudioDeviceEvent;
//     break;

//   case SDL_CONTROLLERAXISMOTION:
//     eventType = "ControllerAxisMotion";
//     logFn = logControllerAxisEvent;
//     break;

//   case SDL_CONTROLLERBUTTONDOWN:
//     eventType = "ControllerButtonDown";
//     logFn = logControllerButtonEvent;

//   case SDL_CONTROLLERBUTTONUP:
//     eventType = "ControllerButtonUp";
//     logFn = logControllerButtonEvent;
//     break;

//   case SDL_CONTROLLERDEVICEADDED:
//     eventType = "ControllerDeviceAdded";
//     logFn = logControllerDeviceEvent;
//     break;

//   case SDL_CONTROLLERDEVICEREMOVED:
//     eventType = "ControllerDeviceRemoved";
//     logFn = logControllerDeviceEvent;
//     break;

//   case SDL_CONTROLLERDEVICEREMAPPED:
//     eventType = "ControllerDeviceRemapped";
//     logFn = logControllerDeviceEvent;
//     break;

//   case SDL_DOLLARGESTURE:
//     eventType = "DollarGesture";
//     logFn = logDollarGestureEvent;
//     break;

//   case SDL_DOLLARRECORD:
//     eventType = "DollarRecord";
//     logFn = logDollarGestureEvent;
//     break;

//   case SDL_DROPFILE:
//     eventType = "DropFile";
//     logFn = logDropEvent;
//     break;

//   case SDL_DROPTEXT:
//     eventType = "DropText";
//     logFn = logDropEvent;
//     break;

//   case SDL_DROPBEGIN:
//     eventType = "DropBegin";
//     logFn = logDropEvent;
//     break;

//   case SDL_DROPCOMPLETE:
//     eventType = "DropComplete";
//     logFn = logDropEvent;
//     break;

//   case SDL_FINGERMOTION:
//     eventType = "FingerMotion";
//     logFn = logTouchFingerEvent;
//     break;

//   case SDL_FINGERDOWN:
//     eventType = "FingerDown";
//     logFn = logTouchFingerEvent;
//     break;

//   case SDL_FINGERUP:
//     eventType = "FingerUp";
//     logFn = logTouchFingerEvent;
//     break;

//   case SDL_KEYDOWN:
//     eventType = "KeyDown";
//     logFn = logKeyboardEvent;
//     break;

//   case SDL_KEYUP:
//     eventType = "KeyUp";
//     logFn = logKeyboardEvent;
//     break;

//   case SDL_JOYAXISMOTION:
//     eventType = "JoyAxisMotion";
//     logFn = logJoyAxisEvent;
//     break;

//   case SDL_JOYBALLMOTION:
//     eventType = "JoyBallMotion";
//     logFn = logJoyBallEvent;
//     break;

//   case SDL_JOYHATMOTION:
//     eventType = "JoyHatMotion";
//     logFn = logJoyHatEvent;
//     break;

//   case SDL_JOYBUTTONDOWN:
//     eventType = "JoyButtonDown";
//     logFn = logJoyButtonEvent;
//     break;

//   case SDL_JOYBUTTONUP:
//     eventType = "JoyButtonUp";
//     logFn = logJoyButtonEvent;
//     break;

//   case SDL_JOYDEVICEADDED:
//     eventType = "JoyDeviceAdded";
//     logFn = logJoyDeviceEvent;
//     break;

//   case SDL_JOYDEVICEREMOVED:
//     eventType = "JoyDeviceRemoved";
//     logFn = logJoyDeviceEvent;
//     break;

//   case SDL_MOUSEMOTION:
//     eventType = "MouseMotion";
//     logFn = logMouseMotionEvent;
//     break;

//   case SDL_MOUSEBUTTONDOWN:
//     eventType = "MouseButtonDown";
//     logFn = logMouseButtonEvent;

//   case SDL_MOUSEBUTTONUP:
//     eventType = "MouseButtonUp";
//     logFn = logMouseButtonEvent;
//     break;

//   case SDL_MOUSEWHEEL:
//     eventType = "MouseWheel";
//     logFn = logMouseWheelEvent;
//     break;

//   case SDL_MULTIGESTURE:
//     eventType = "MultiGesture";
//     logFn = logMultiGestureEvent;
//     break;

//   case SDL_QUIT:
//     eventType = "Quit";
//     logFn = logQuitEvent;
//     break;

//   case SDL_SYSWMEVENT:
//     eventType = "SysWMEvent";
//     logFn = logSysWMEvent;
//     break;

//   case SDL_TEXTEDITING:
//     eventType = "TextEditing";
//     logFn = logTextEditingEvent;
//     break;

//   case SDL_TEXTINPUT:
//     eventType = "TextInput";
//     logFn = logTextInputEvent;
//     break;

//   case SDL_USEREVENT:
//     eventType = "UserEvent";
//     logFn = logUserEvent;
//     break;

//   case SDL_WINDOWEVENT:
//     eventType = "WindowEvent";
//     logFn = logWindowEvent;
//     break;

//   default:
//     eventType = "CommonEvent";
//     logFn = logCommonEvent;
//     break;
//   }

//   logFn(eventType, event);
// }

void Logger::logEventInternal(Uint32 timestamp, std::string type, std::string str) {
  std::cout << "event: { t=" << timestamp << ", "
            << "type=" << type << ", "
            << str
            << " }" << std::endl;
}

void Logger::logAudioDeviceEvent(SDL_Event event) {

}

void Logger::logControllerAxisEvent(SDL_Event event) {

}

void Logger::logControllerButtonEvent(SDL_Event event) {

}

void Logger::logControllerDeviceEvent(SDL_Event event) {

}

void Logger::logDollarGestureEvent(SDL_Event event) {

}

void Logger::logDropEvent(SDL_Event event) {

}

void Logger::logTouchFingerEvent(SDL_Event event) {

}

void Logger::logKeyboardEvent(SDL_Event event) {
  std::string type = "";
  if (event.type == SDL_KEYUP) {
    type = "KeyUp";
  } else if (event.type == SDL_KEYDOWN) {
    type = "KeyDown";
  }

  if (!type.empty()) {
    std::string scancode = SDL_GetScancodeName(event.key.keysym.scancode);
    std::string keyname = SDL_GetKeyName(event.key.keysym.sym);
    std::string mod = std::to_string(event.key.keysym.mod);
    std::string state = "";
    if (event.key.state == SDL_PRESSED) {
      state = "pressed";
    } else if (event.key.state == SDL_RELEASED) {
      state = "released";
    }
    std::string str = std::string("scancode=") + scancode +
      ", keyname=" + keyname +
      ", mod=" + mod +
      ", state=" + state;
    logEventInternal(event.key.timestamp, type, str);
  }
}

void Logger::logJoyAxisEvent(SDL_Event event) {

}

void Logger::logJoyBallEvent(SDL_Event event) {

}

void Logger::logJoyButtonEvent(SDL_Event event) {

}

void Logger::logJoyDeviceEvent(SDL_Event event) {

}

void Logger::logJoyHatEvent(SDL_Event event) {

}

void Logger::logMouseMotionEvent(SDL_Event event) {

}

void Logger::logMouseButtonEvent(SDL_Event event) {
  std::string type = "";
  if (event.type == SDL_MOUSEBUTTONDOWN) {
    type = "MouseButtonDown";
  } else if (event.type == SDL_MOUSEBUTTONUP) {
    type = "MouseButtonUp";
  }

  if (!type.empty()) {
    std::string button = "";
    if (event.button.button == SDL_BUTTON_LEFT) {
      button = "left";
    } else if (event.button.button == SDL_BUTTON_MIDDLE) {
      button = "middle";
    } else if (event.button.button == SDL_BUTTON_RIGHT) {
      button = "right";
    }

    std::string str = std::string("button=") + button;
    logEventInternal(event.button.timestamp, type, str);
  }
}

void Logger::logMouseWheelEvent(SDL_Event event) {

}

void Logger::logMultiGestureEvent(SDL_Event event) {

}

void Logger::logQuitEvent(SDL_Event event) {

}

void Logger::logSysWMEvent(SDL_Event event) {

}

void Logger::logTextEditingEvent(SDL_Event event) {

}

void Logger::logTextInputEvent(SDL_Event event) {

}

void Logger::logUserEvent(SDL_Event event) {

}

void Logger::logWindowEvent(SDL_Event event) {

}

void Logger::logCommonEvent(SDL_Event event) {

}
