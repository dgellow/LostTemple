#include "animated_graphic.hpp"
#include <SDL.h>

AnimatedGraphic::AnimatedGraphic() : SDLGameObject() {

}

void AnimatedGraphic::draw() {
  SDLGameObject::draw();
}

void AnimatedGraphic::update() {
  currentFrame = int((SDL_GetTicks() / (1000 / animSpeed)) % numFrames);
}

void AnimatedGraphic::clean() {
  SDLGameObject::clean();
}

void AnimatedGraphic::load(const LoaderParams* pParams) {
  SDLGameObject::load(pParams);
  animSpeed = pParams->getAnimSpeed();
}
