#ifndef __MenuState__
#define __MenuState__

#include <vector>
#include "game_state.hpp"
#include "game_object.hpp"

class MenuState : public GameState {
public:
  virtual ~MenuState() {}

  virtual void update();
  virtual void render();
  virtual bool onEnter();
  virtual bool onExit();
  virtual std::string getStateId() const =0;

protected:
  typedef void(*Callback) ();
  virtual void setCallbacks(const std::vector<Callback> &callbacks)=0;
  std::vector<Callback> callbacks;
  std::vector<GameObject*> gameObjects;
};

#endif /* defined(__MenuState__) */
