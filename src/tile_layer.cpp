#include "tile_layer.hpp"
#include "game.hpp"
#include "texture_manager.hpp"

TileLayer::TileLayer(int tileSize, const std::vector<Tileset> &tilesets) :
  tileSize(tileSize), tilesets(tilesets), position(0, 0), velocity(0, 0) {

  numColumns = Game::instance()->getGameWidth() / tileSize;
  numRows = Game::instance()->getGameHeight() / tileSize;
}

void TileLayer::update() {
  float x = position.getX() / tileSize;
  float y = position.getY() / tileSize;

  if (x <= 0) {
    position.setX(0);
    velocity.setX(1);
  } else if (x + numColumns > this->width) {
    position.setX((this->width - numColumns) * tileSize);
    velocity.setX(-1);
  }

  if (y < 0) {
    position.setY(0);
  } else if (y + numRows > this->height) {
    position.setY((this->height - - numRows) * tileSize);
  }


  position += velocity;
}

void TileLayer::render() {
  int x, y, x2, y2;
  x = y = x2 = y2 = 0;

  // Position in the tilelayer of the screen
  x = position.getX() / tileSize;
  y = position.getY() / tileSize;

  // Offset, to have a smooth scrolling
  x2 = int(position.getX()) % tileSize;
  y2 = int(position.getY()) % tileSize;

  if (layerId == "platforms" && x % 60 == 0) {
    std::cout << "TileLayer::render():"
              << " name=" << layerId
              << " x=" << x
              << " y=" << y
              << " x2=" << x2
              << " y2=" << y2
              << " width=" << getWidth()
              << " height=" << getHeight()
              << std::endl;
  }

  for (int i = 0; i < numRows; i++) {
    for (int j = 0; j < numColumns; j++) {

      // TODO: At the moment the game will crash if the window height
      // or width is bigger than the level
      // ex: (window.height / tileSize) > numRows

      int id = tileIds[int(i + y) % getHeight()][int(j + x) % getWidth()];

      if (layerId == "platforms" && x % 60 == 0) {
        std::cout << id;
      }

      if (j + x > getWidth() || i + y > getHeight() || id == 0) {
        continue;
      }

      Tileset tileset = getTilesetById(id);
      id--;

      // x and y on screen
      int xpos = (j * tileSize) - x2;
      int ypos = (i * tileSize) - y2;
      // Row in spritesheet
      int currentRow = (id - (tileset.firstGridID - 1)) / tileset.numColumns;
      // Column in spritesheet
      int currentFrame = (id - (tileset.firstGridID - 1)) % tileset.numColumns;
      // Draw texture
      TextureManager::instance()->drawTile(tileset.name,
                                           tileset.margin, tileset.spacing,
                                           xpos, ypos, tileSize, tileSize,
                                           currentRow, currentFrame,
                                           Game::instance()->getRenderer());
    }

    if (layerId == "platforms" && x % 60 == 0) {
      std::cout << std::endl;
    }
  }
}


Tileset TileLayer::getTilesetById(int tileId)  {
  for (size_t i = 0; i < tilesets.size(); i++) {
    if (i + 1 <= tilesets.size() - 1) {
      if (tileId >= tilesets[i].firstGridID && tileId < tilesets[i + 1].firstGridID) {
        return tilesets[i];
      }
    } else {
      return tilesets[i];
    }
  }

  std::cerr << "Did not find tileset: " << tileId << ". Returning empty tileset"
            << std::endl;
  Tileset t;
  return t;
}
