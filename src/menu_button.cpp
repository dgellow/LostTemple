#include "menu_button.hpp"
#include "input_handler.hpp"
#include <iostream>

MenuButton::MenuButton() : SDLGameObject() {

}

void MenuButton::draw() {
  // std::cout << "MenuButton::draw() -> tick" << std::endl;

  SDLGameObject::draw();
}

void MenuButton::update() {
  // std::cout << "MenuButton::update() -> tick" << std::endl;

  Vector2D* pMousePos = InputHandler::instance()->getMousePosition();

  if (pMousePos->getX() < (position.getX() + width) &&
      pMousePos->getX() > position.getX() &&
      pMousePos->getY() < (position.getY() + height) &&
      pMousePos->getY() > position.getY()) {

    if (InputHandler::instance()->getMouseButtonState(MOUSE_LEFT) && isReleased) {
      currentFrame = CLICKED;
      callback();
      isReleased = false;
    } else if (!InputHandler::instance()->getMouseButtonState(MOUSE_LEFT)) {
      isReleased = true;
      currentFrame = MOUSE_OVER;
    }
  } else {
    currentFrame = MOUSE_OUT;
  }
}

void MenuButton::clean() {
  std::cout << "MenuButton::clean()" << std::endl;

  SDLGameObject::clean();
}

void MenuButton::load(const LoaderParams* pParams) {
  std::cout << "MenuButton::load()" << std::endl;

  SDLGameObject::load(pParams);
  callbackId = pParams->getCallbackId();
  currentFrame = MOUSE_OUT;
}
