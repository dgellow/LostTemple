#ifndef __Const__
#define __Const__

const int FPS = 60;
const int DELAY_TIME = 1000.0f / FPS;
const short COLOR_BG_R = 0;
const short COLOR_BG_G = 0;
const short COLOR_BG_B = 0;
const short COLOR_BG_A = 255;

#endif /* defined(__Const__) */
