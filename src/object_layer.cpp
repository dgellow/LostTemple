#include "object_layer.hpp"

void ObjectLayer::update() {
  for (size_t i = 0; i < gameObjects.size(); i++) {
    gameObjects[i]->update();
  }
}

void ObjectLayer::render() {
  for (size_t i = 0; i < gameObjects.size(); i++) {
    gameObjects[i]->draw();
  }
}
