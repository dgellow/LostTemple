#include "input_handler.hpp"
#include <iostream>
#include "game.hpp"
#include "logger.hpp"

InputHandler* InputHandler::pInstance = 0;

InputHandler::InputHandler() {
  mousePosition = new Vector2D(0, 0);
  for(int i = 0; i < 3; i++) {
    mouseButtonStates.push_back(false);
  }
}

InputHandler::~InputHandler() {

}

InputHandler* InputHandler::instance() {
  if (pInstance == 0) {
    pInstance = new InputHandler();
  }
  return pInstance;
}

void InputHandler::initializeJoysticks(){
  if (SDL_WasInit(SDL_INIT_JOYSTICK) == 0) {
    SDL_InitSubSystem(SDL_INIT_JOYSTICK);
  }

  if (SDL_NumJoysticks() > 0) {
    for (int i = 0; i < SDL_NumJoysticks(); i++) {
      SDL_Joystick* joy = SDL_JoystickOpen(i);

      if (joy) {
        joysticks.push_back(joy);
        joystickValues.push_back(std::make_pair(new Vector2D(0, 0),
                                                new Vector2D(0, 0)));

        std::vector<bool> tempButtons;
        for (int j = 0; j < SDL_JoystickNumButtons(joy); j++) {
          tempButtons.push_back(false);
        }
        buttonStates.push_back(tempButtons);
      } else {
        std::cout << SDL_GetError();
      }
    }

    SDL_JoystickEventState(SDL_ENABLE);
    areJoysticksInitialized = true;

    std::cout << "Initialized " << joysticks.size() << " joystick(s)\n";
  } else {
    areJoysticksInitialized = false;
  }
}

void InputHandler::clean() {
  if (areJoysticksInitialized) {
    for (int i = 0; i < SDL_NumJoysticks(); i++) {
      SDL_JoystickClose(joysticks[i]);
    }
  }
}

void InputHandler::update() {
  keystates = (Uint8*) SDL_GetKeyboardState(0);

  SDL_Event event;
  while(SDL_PollEvent(&event)) {
    // Logging
    Logger::logMouseButtonEvent(event);
    // Logger::logJoyButtonEvent(event);
    Logger::logKeyboardEvent(event);

    // Handle events
    switch(event.type) {

    case SDL_QUIT:
      Game::instance()->quit();
      break;

    case SDL_JOYBUTTONDOWN:
      onJoystickButtonDown(event);
      break;

    case SDL_JOYBUTTONUP:
      onJoystickButtonUp(event);
      break;

    case SDL_JOYAXISMOTION:
      onJoystickAxisMove(event);
      break;

    case SDL_MOUSEBUTTONDOWN:
      onMouseButtonDown(event);
      break;

    case SDL_MOUSEBUTTONUP:
      onMouseButtonUp(event);
      break;

    case SDL_MOUSEMOTION:
      onMouseMove(event);
      break;

    case SDL_KEYDOWN:
      onKeyDown();
      break;

    case SDL_KEYUP:
      onKeyUp();
      break;

    default:
      break;
    }
  }
}

int InputHandler::xvalue(int joy, int stick) {
  if (joystickValues.size() > 0) {
    if (stick == 1) {
      return joystickValues[joy].first->getX();
    } else if (stick == 2) {
      return joystickValues[joy].second->getX();
    }
  }

  return 0;
}

int InputHandler::yvalue(int joy, int stick) {
  if (joystickValues.size() > 0) {
    if (stick == 1) {
      return joystickValues[joy].first->getY();
    } else if (stick == 2) {
      return joystickValues[joy].second->getY();
    }
  }

  return 0;
}

bool InputHandler::getButtonState(int joy, int buttonNumber) {
  return buttonStates[joy][buttonNumber];
}

bool InputHandler::getMouseButtonState(int buttonNumber) {
  return mouseButtonStates[buttonNumber];
}

Vector2D* InputHandler::getMousePosition() {
  return mousePosition;
}

bool InputHandler::isKeyDown(SDL_Scancode key) {
  if (keystates != 0) {
    if (keystates[key] == 1) {
      return true;
    } else {
      return false;
    }
  }

  return false;
}

void InputHandler::onKeyDown() {

}

void InputHandler::onKeyUp() {

}

void InputHandler::onMouseMove(SDL_Event& event) {
  mousePosition->setX(event.motion.x);
  mousePosition->setY(event.motion.y);
}

void InputHandler::onMouseButtonDown(SDL_Event& event) {
  if (event.button.button == SDL_BUTTON_LEFT) {
    mouseButtonStates[MOUSE_LEFT] = true;
  }

  if (event.button.button == SDL_BUTTON_MIDDLE) {
    mouseButtonStates[MOUSE_MIDDLE] = true;
  }

  if (event.button.button == SDL_BUTTON_RIGHT) {
    mouseButtonStates[MOUSE_RIGHT] = true;
  }

}

void InputHandler::onMouseButtonUp(SDL_Event& event) {
  if (event.button.button == SDL_BUTTON_LEFT) {
    mouseButtonStates[MOUSE_LEFT] = false;
  }

  if (event.button.button == SDL_BUTTON_MIDDLE) {
    mouseButtonStates[MOUSE_MIDDLE] = false;
  }

  if (event.button.button == SDL_BUTTON_RIGHT) {
    mouseButtonStates[MOUSE_RIGHT] = false;
  }
}

void InputHandler::onJoystickAxisMove(SDL_Event& event) {
  int whichOne = event.jaxis.which;

  // left stick horizontal move left or right
  if (event.jaxis.axis == 0) {
    if (event.jaxis.value > joystickDeadZone) {
      joystickValues[whichOne].first->setX(1);
    } else if (event.jaxis.value < -joystickDeadZone) {
      joystickValues[whichOne].first->setX(-1);
    } else {
      joystickValues[whichOne].first->setX(0);
    }
  }

  // left stick vertical move up or down
  if (event.jaxis.axis == 1) {
    if (event.jaxis.value > joystickDeadZone) {
      joystickValues[whichOne].first->setY(1);
    } else if (event.jaxis.value < -joystickDeadZone) {
      joystickValues[whichOne].first->setY(-1);
    } else {
      joystickValues[whichOne].first->setY(0);
    }
  }

  // right stick horizontal move left or right
  if (event.jaxis.axis == 2) {
    if (event.jaxis.value > joystickDeadZone) {
      joystickValues[whichOne].second->setX(1);
    } else if (event.jaxis.value < -joystickDeadZone) {
      joystickValues[whichOne].second->setX(-1);
    } else {
      joystickValues[whichOne].second->setX(0);
    }
  }

  // right stick vertical move up or down
  if (event.jaxis.axis == 3) {
    if (event.jaxis.value > joystickDeadZone) {
      joystickValues[whichOne].second->setY(1);
    } else if (event.jaxis.value < -joystickDeadZone) {
      joystickValues[whichOne].second->setY(-1);
    } else {
      joystickValues[whichOne].second->setY(0);
    }
  }
}

void InputHandler::onJoystickButtonDown(SDL_Event& event) {
  int whichOne = event.jaxis.which;
  buttonStates[whichOne][event.jbutton.button] = true;
}

void InputHandler::onJoystickButtonUp(SDL_Event& event) {
  int whichOne = event.jaxis.which;
  buttonStates[whichOne][event.jbutton.button] = false;
}

void InputHandler::reset() {
  for(size_t i = 0; i < mouseButtonStates.size(); i++) {
    mouseButtonStates[i] = false;
  }
}
