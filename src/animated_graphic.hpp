#ifndef __AnimatedGraphic__
#define __AnimatedGraphic__

#include "sdl_game_object.hpp"
#include "loader_params.hpp"
#include "base_creator.hpp"
#include "game_object.hpp"

class AnimatedGraphic : public SDLGameObject {
public:
  AnimatedGraphic();

  virtual void draw();
  virtual void update();
  virtual void clean();
  virtual void load(const LoaderParams* pParams);

private:
  int animSpeed;
};

class AnimatedGraphicCreator : public BaseCreator {
private:
  GameObject* createGameObject() const {
    return new AnimatedGraphic();
  }
};

#endif /* defined(__AnimatedGraphic__) */
