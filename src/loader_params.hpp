#ifndef __LoaderParams__
#define __LoaderParams__

#include <string>
#include "loader_params.hpp"

class LoaderParams {
public:
  LoaderParams(int x, int y, int width, int height, std::string textureId,
               int numFrames = 1, int callbackId = 0, int animSpeed = 0) :
    x(x),
    y(y),
    width(width),
    height(height),
    textureId(textureId),
    numFrames(numFrames),
    callbackId(callbackId),
    animSpeed(animSpeed) {

  }

  int getX() const {return x;}
  int getY() const {return y;}
  int getWidth() const {return width;}
  int getHeight() const {return height;}
  std::string getTextureId() const {return textureId;}
  int getNumFrames() const {return numFrames;}
  int getCallbackId() const {return callbackId;}
  int getAnimSpeed() const {return animSpeed;}

protected:
  int x;
  int y;
  int width;
  int height;
  int numFrames;
  int callbackId;
  int animSpeed;
  std::string textureId;
};

#endif /* defined(__LoaderParams__) */
