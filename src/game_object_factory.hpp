#ifndef __GameObjectFactory__
#define __GameObjectFactory__

#include <iostream>
#include <string>
#include <map>
#include "game_object.hpp"
#include "base_creator.hpp"

class GameObjectFactory {
public:
  static GameObjectFactory* instance();
  bool registerType(std::string typeId, BaseCreator* pCreator);
  GameObject* create(std::string typeId);

private:
  GameObjectFactory() {}
  ~GameObjectFactory() {}

  static GameObjectFactory* pInstance;
  std::map<std::string, BaseCreator*> creators;
};

#endif /* defined(__GameObjectFactory__) */
