#include <iostream>
#include <vector>
#include "tinyxml2.hpp"

#include "game_object.hpp"

class StateParser {
public:
  bool parseState(const char* stateFile, std::string stateId,
                  std::vector<GameObject*> *pObjects,
                  std::vector<std::string> *pTextureIds);

private:
  void parseObjects(tinyxml2::XMLElement* pStateRoot,
                    std::vector<GameObject*> *pObjects);
  void parseTextures(tinyxml2::XMLElement* pStateRoot,
                     std::vector<std::string> *pTextureIds);
};
