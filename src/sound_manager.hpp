#ifndef __SoundManager__
#define __SoundManager__

#include <string>
#include <map>
#include <SDL_mixer.h>

enum SoundTypes {
  SOUND_MUSIC = 0,
  SOUND_SFX = 1
};

class SoundManager {
public:
  static SoundManager* instance();

  bool load(std::string fileName, std::string id, SoundTypes type);
  void playSound(std::string id, int loop);
  void playMusic(std::string id, int loop);

private:
  static SoundManager* pInstance;

  SoundManager();
  virtual ~SoundManager();

  std::map<std::string, Mix_Chunk*> sfxs;
  std::map<std::string, Mix_Music*> music;

  SoundManager(const SoundManager&);
  SoundManager &operator=(const SoundManager&);
};

#endif /* defined(__SoundManager__) */
