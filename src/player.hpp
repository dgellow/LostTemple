#ifndef __Player__
#define __Player__

#include "sdl_game_object.hpp"
#include "loader_params.hpp"
#include "base_creator.hpp"

class Player : public SDLGameObject {
public:
  Player();

  virtual void draw();
  virtual void update();
  virtual void clean();
  virtual void load(const LoaderParams* pParams);

private:
  void handleInput();
};

class PlayerCreator : public BaseCreator {
private:
  GameObject* createGameObject() const {
    return new Player();
  }
};

#endif /* defined(__Player__) */
