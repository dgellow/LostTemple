#include "sound_manager.hpp"
#include <iostream>

SoundManager::SoundManager() {
  int frequency = 22050;
  int chunksize = 4096;
  Uint16 format = AUDIO_S16;
  int channels = 2;

  Mix_OpenAudio(frequency, format, channels, chunksize);
}

SoundManager::~SoundManager() {
  Mix_CloseAudio();
}

bool SoundManager::load(std::string fileName, std::string id, SoundTypes type) {
  if (type == SOUND_MUSIC) {
    Mix_Music* pMusic = Mix_LoadMUS(fileName.c_str());

    if (pMusic == 0) {
      std::cerr << "Error when loading music: " << Mix_GetError() << std::endl;
      return false;
    }

    music[id] = pMusic;
    return true;
  } else if (type == SOUND_SFX) {
    Mix_Chunk* pChunk = Mix_LoadWAV(fileName.c_str());
    if (pChunk == 0) {
      std::cerr << "Error when loading SFX: " << Mix_GetError() << std::endl;
      return false;
    }

    sfxs[id] = pChunk;
    return true;
  }

  return false;
}

void SoundManager::playSound(std::string id, int loop) {
  Mix_PlayChannel(-1, sfxs[id], loop);
}

void SoundManager::playMusic(std::string id, int loop) {
  Mix_PlayMusic(music[id], loop);
}
