#include "state_parser.hpp"
#include "texture_manager.hpp"
#include "game.hpp"
#include "game_object_factory.hpp"

bool StateParser::parseState(const char *stateFile, std::string stateId,
                             std::vector<GameObject *> *pObjects,
                             std::vector<std::string> *pTextureIds) {

  std::cout << "Start parsing state file: " << stateFile << std::endl;

  // Create the XML document
  tinyxml2::XMLDocument xmlDoc;

  // Load the state file
  xmlDoc.LoadFile(stateFile);
  if (xmlDoc.Error()) {
    std::cerr << "Error loading file: " << stateFile << std::endl;
    std::cerr << xmlDoc.GetErrorStr1() << std::endl;
    std::cerr << xmlDoc.GetErrorStr2() << std::endl;
    return false;
  }

  // Get the root element
  tinyxml2::XMLElement* pRoot = xmlDoc.RootElement();

  // Pre declare the states root node
  tinyxml2::XMLElement* pStateRoot = 0;

  // Get this states root node and assign it to pStateRoot
  for (tinyxml2::XMLElement* e = pRoot->FirstChildElement();
       e != NULL;
       e = e->NextSiblingElement()) {
    if (e->Value() == stateId) {
      pStateRoot = e;
    }
  }

  if (pRoot == 0) {
    std::cerr << "Cannot find node " << stateId << " in " << stateFile << std::endl;
    return false;
  }

  // Pre declare the texture root
  tinyxml2::XMLElement* pTextureRoot = 0;

  // Get the root of the texture elements
  for (tinyxml2::XMLElement* e = pStateRoot->FirstChildElement();
       e != NULL;
       e = e->NextSiblingElement()) {
    if (e->Value() == std::string("Textures")) {
      pTextureRoot = e;
    }
  }

  // Parse the textures
  parseTextures(pTextureRoot, pTextureIds);

  // Pre declare the object root node
  tinyxml2::XMLElement* pObjectRoot = 0;

  // Get the root node and assign it to pObjectRoot
  for (tinyxml2::XMLElement* e = pStateRoot->FirstChildElement();
       e != NULL;
       e = e->NextSiblingElement()) {
    if (e->Value() == std::string("Objects")) {
      pObjectRoot = e;
    }
  }

  // Parse the objects
  parseObjects(pObjectRoot, pObjects);

  std::cout << "End parsing state file: " << stateFile << std::endl;

  return true;
}

void StateParser::parseObjects(tinyxml2::XMLElement *pStateRoot,
                               std::vector<GameObject *> *pObjects) {

  std::cout << "\tStart parsing objects" << std::endl;

  for (tinyxml2::XMLElement* e = pStateRoot->FirstChildElement();
       e != NULL;
       e = e->NextSiblingElement()) {

    int x = e->IntAttribute("x");
    int y = e->IntAttribute("y");
    int width = e->IntAttribute("width");
    int height = e->IntAttribute("height");
    int numFrames = e->IntAttribute("numFrames");
    int callbackId = e->IntAttribute("callbackId");
    int animSpeed = e->IntAttribute("animSpeed");
    std::string textureId = e->Attribute("textureId");

    std::string type = e->Attribute("type");

    GameObject* pGameObject = GameObjectFactory::instance()->create(type);
    pGameObject->load(new LoaderParams(x, y, width, height, textureId, numFrames,
                                       callbackId, animSpeed));
    pObjects->push_back(pGameObject);
  }

  std::cout << "\tEnd parsing objects" << std::endl;
}

void StateParser::parseTextures(tinyxml2::XMLElement *pStateRoot,
                                std::vector<std::string> *pTextureIds) {

  std::cout << "\tStart parsing textures" << std::endl;

  for (tinyxml2::XMLElement* e = pStateRoot->FirstChildElement();
       e != NULL;
       e = e->NextSiblingElement()) {

    std::string filenameAttribute = e->Attribute("filename");
    std::string idAttribute = e->Attribute("id");
    pTextureIds->push_back(idAttribute);

    TextureManager::instance()->load(filenameAttribute, idAttribute,
                                     Game::instance()->getRenderer());
  }

  std::cout << "End parsing textures" << std::endl;
}
