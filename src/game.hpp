#ifndef __Game__
#define __Game__

#include <iostream>
#include <vector>
#include <SDL.h>
#include <SDL_image.h>
#include "game_state_machine.hpp"
#include "game_object.hpp"

enum GameStates {
  MENU = 0,
  PLAY = 1,
  GAMEOVER = 2
};

class Game {
public:
  static Game* instance();

  bool init(const char* title, int xpos, int ypos,
            int width, int height, bool fullscreen);
  void render();
  void update();
  void handleEvents();
  void clean();
  void quit();

  bool running() { return isRunning; }
  SDL_Renderer* getRenderer() const {return pRenderer;}
  GameStateMachine* getStateMachine() const {return pGameStateMachine;}

  int getGameWidth() const {
    return gameWidth;
  }

  int getGameHeight() const {
    return gameHeight;
  }

private:
  Game();
  static Game* pInstance;

  SDL_Window* pWindow;
  SDL_Renderer* pRenderer;

  bool isRunning;
  int gameWidth;
  int gameHeight;

  bool initSDL(const char* title, int xpos, int ypos, bool fullscreen);
  std::vector<GameObject*> gameObjects;
  GameStateMachine* pGameStateMachine;
};

#endif /* defined(__Game__) */
