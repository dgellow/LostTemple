#include "sdl_game_object.hpp"
#include "game.hpp"
#include "texture_manager.hpp"
#include <iostream>

SDLGameObject::SDLGameObject() {

}

void SDLGameObject::load(const LoaderParams* pParams) {
  position = Vector2D(pParams->getX(), pParams->getY());
  velocity = Vector2D(0, 0);
  acceleration = Vector2D(0, 0);
  width = pParams->getWidth();
  height = pParams->getHeight();
  textureId = pParams->getTextureId();

  numFrames = pParams->getNumFrames();
  currentRow = 1;
  currentFrame = 1;
}

void SDLGameObject::draw() {
  // std::cout << "SDLGameObject::draw(): position=" << position
  //           << ", width=" << width
  //           << ", height=" << height
  //           << std::endl;

  SDL_RendererFlip flip = velocity.getX() > 0 ? SDL_FLIP_HORIZONTAL: SDL_FLIP_NONE;
  TextureManager::instance()->drawFrame(textureId,
                                        (int) position.getX(),
                                        (int) position.getY(),
                                        width, height,
                                        currentRow, currentFrame,
                                        Game::instance()->getRenderer(),
                                        flip);
}

void SDLGameObject::update() {
  velocity += acceleration;
  position += velocity;

  // std::cout << "SDLGameObject::update(): acceleration=" << acceleration
  //           << ", velocity=" << velocity << std::endl;
}

void SDLGameObject::clean() {

}
