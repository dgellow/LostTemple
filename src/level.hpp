#ifndef __Level__
#define __Level__

#include <string>
#include <vector>
#include "layer.hpp"

struct Tileset {
  int firstGridID;
  int tileWidth;
  int tileHeight;
  int spacing;
  int margin;
  int width;
  int height;
  int numColumns;
  std::string name;
  std::string source;
};

class Level {
public:
  Level() {}
  ~Level() {}

  void update();
  void render();

  std::vector<Tileset>* getTilesets() {
    return &tilesets;
  }

  std::vector<Layer*>* getLayers() {
    return &layers;
  }

private:
  std::vector<Tileset> tilesets;
  std::vector<Layer*> layers;
};

#endif /* defined(__Level__) */
