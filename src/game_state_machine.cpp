#include "game_state_machine.hpp"

void GameStateMachine::pushState(GameState *pState) {
  gameStates.push_back(pState);
  gameStates.back()->onEnter();
}

void GameStateMachine::popState() {
  if (!gameStates.empty()) {
    if (gameStates.back()->onExit()) {
      delete gameStates.back();
      gameStates.pop_back();
    }
  }
}

void GameStateMachine::changeState(GameState *pState) {
  if (!gameStates.empty()) {
    if (gameStates.back()->getStateId() == pState->getStateId()) {
      return;
    }

    gameStates.back()->onExit();
    gameStates.pop_back();
  }

  pState->onEnter();
  gameStates.push_back(pState);
}

void GameStateMachine::update() {
  if (!gameStates.empty()) {
    gameStates.back()->update();
  }
}

void GameStateMachine::render() {
  if (!gameStates.empty()) {
    gameStates.back()->render();
  }
}
