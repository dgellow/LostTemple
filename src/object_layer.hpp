#ifndef __ObjectLayer__
#define __ObjectLayer__

#include <vector>
#include <string>
#include "game_object.hpp"
#include "level.hpp"
#include "layer.hpp"
#include "vector_2d.hpp"

class ObjectLayer : public Layer {
public:
  virtual void update();
  virtual void render();

  std::string getLayerId() const {
    return layerId;
  }

  void setLayerId(std::string layerId) {
    this->layerId = layerId;
  }

  int getWidth() const {
    return width;
  }

  void setWidth(int width) {
    this->width = width;
  }

  int getHeight() const {
    return height;
  }

  void setHeight(int height) {
    this->height = height;
  }

  std::vector<GameObject*>* getGameObjects() {
    return &gameObjects;
  }

private:
  std::vector<GameObject*> gameObjects;
  std::string layerId;
  int width;
  int height;
};

#endif /* defined(__ObjectLayer__) */
