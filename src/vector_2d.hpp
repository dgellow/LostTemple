#ifndef __Vector2D__
#define __Vector2D__

#include <math.h>
#include <string>
#include <ostream>

class Vector2D {
public:
  Vector2D() : x(0), y(0) {}
  Vector2D(float x, float y) : x(x), y(y) {}

  operator std::string() const {
    return "{x: " + std::to_string(x) + + ", y: " + std::to_string(y) + "}";
  }
  friend std::ostream & operator<<(std::ostream & stream, Vector2D const & v) {
    return stream << std::string(v);
  }

  float getX() {return x;}
  float getY() {return y;}
  void setX(float x) {this->x = x;}
  void setY(float y) {this->y = y;}

  float length() {
    return sqrt(x * x + y * y);
  }

  Vector2D operator+(const Vector2D& v2) const {
    return Vector2D(x + v2.x, y + v2.y);
  }

  friend Vector2D& operator+=(Vector2D& v1, const Vector2D& v2) {
    v1.x += v2.x;
    v1.y += v2.y;
    return v1;
  }

  Vector2D operator*(float scalar) {
    return Vector2D(x * scalar, y * scalar);
  }

  Vector2D& operator*=(float scalar) {
    x *= scalar;
    y *= scalar;
    return *this;
  }

  Vector2D operator-(const Vector2D& v2) const {
    return Vector2D(x - v2.x, y - v2.y);
  }

  friend Vector2D& operator-=(Vector2D& v1, const Vector2D& v2) {
    v1.x -= v2.x;
    v1.y -= v2.y;
    return v1;
  }

  Vector2D operator/(float scalar) {
    return Vector2D(x / scalar, y / scalar);
  }

  Vector2D& operator/=(float scalar) {
    x /= scalar;
    y /= scalar;
    return *this;
  }

  void normalize() {
    float l = length();
    if (l > 0) {
      (*this) *= 1 / l;
    }
  }

private:
  float x;
  float y;
};

#endif /* defined(__Vector2D__) */
