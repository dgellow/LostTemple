#ifndef __GameOverState__
#define __GameOverState__

#include <vector>
#include "menu_state.hpp"
#include "game_object.hpp"

class GameOverState : public MenuState {
public:
  virtual void update();
  virtual void render();
  virtual bool onEnter();
  virtual bool onExit();
  virtual std::string getStateId() const {return stateId;}

private:
  virtual void setCallbacks(const std::vector<Callback> &callbacks);

  // callbacks for menu items
  static void gameOverToMain();
  static void restartPlay();

  static const std::string stateId;
  std::vector<GameObject*> gameObjects;
};

#endif /* defined(__GameOverState__) */
